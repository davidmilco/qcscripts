//Dependencies needed contained in node_modules
var soap = require('soap');
var http = require('http');
var mysql = require('mysql');

//Constant Variables provided by LQB
var lqbLoanUrl = 'https://webservices.lendingqb.com//los/webservice/Loan.asmx?WSDL';
var args = { "userName": 'cabo', "passWord": 'group61]trim' };
var lqbAuthUrl = 'https://webservices.lendingqb.com/los/webservice/AuthService.asmx?WSDL';
var reportUrl = 'http://api.openmtg.com/customReport/qcLoanWorkConvHighRisk';
var requestDataString = '';
var secondRequestDataString = '';
var thirdRequestDataString = ''


console.log('QCAuotmation for Conventional Loans Function Run');

//Get total number of Conventional Loans for the month
var totalMonthDataURL = 'http://api.openmtg.com/customReport/QcTotalConv';
var totalLoansThisMonth = 1;
var totalDataFunction = function (next) {
    console.log('totalDataFunction Called Part 1');
    http.get(totalMonthDataURL, function (res) {
        //In case of large amounts of data using chunkBuffer and combining as it goes
        res.on('data', function (chunkBuffer) {
            requestDataString += chunkBuffer.toString();
        });

        // On response end, break the report data into a groups of one array per line, ignore the first 3 as they are the report name and column names
        res.on('end', function () {
            var splitLinesArray = requestDataString.split('\n');
            var removedTopLines = splitLinesArray.splice(4, splitLinesArray.length);
            var tempHolder = [];
            //For each line split it into an array with each value being the column data.  Grab the loan number and put into a tempHolder array.
            for (var y = 0; y < removedTopLines.length; y++) {
                var singleLine = removedTopLines[y].split(',');
                var loanNumber = singleLine[0].replace(/['"]+/g, '');
                tempHolder.push(loanNumber);
            }
            console.log('Total loans from the total loans check: ', tempHolder.length);
            console.log(tempHolder);
            totalLoansThisMonth = tempHolder.length;
            next();
        })
    })
}

//Get number of loans that have been completed or flagged.
var flaggedMonthDataURL = 'http://api.openmtg.com/customReport/flaggedConvMonth'; //52 Checked or 52 Date 
var totalFlaggedLoansThisMonth = 0;
var flaggedDataFunction = function () {
    console.log('flaggedDataFunction Called Part 2');
    http.get(flaggedMonthDataURL, function (res) {
        res.on('data', function (chunkBuffer) {
            secondRequestDataString += chunkBuffer.toString();
        });
        res.on('end', function () {
            var splitLinesArray = secondRequestDataString.split('\n');
            var removedTopLines = splitLinesArray.splice(4, splitLinesArray.length);
            var tempHolder = [];
            //For each line split it into an array with each value being the column data.  Grab the loan number and put into an array.
            for (var y = 0; y < removedTopLines.length; y++) {
                var singleLine = removedTopLines[y].split(',');
                var loanNumber = singleLine[0].replace(/['"]+/g, '');
                tempHolder.push(loanNumber);
            }
            console.log('Total number of flagged loans: ', tempHolder.length);
            totalFlaggedLoansThisMonth = tempHolder.length;
            console.log(tempHolder);
            pickerFunction();
        })

    })

}

//Pull Report and condense data into usable format
var pickerFunction = function () {
    console.log('pickerFunction Called Part 3');
    http.get(reportUrl, function (res) {

        //In case of large amounts of data using chunkBuffer and combining as it goes
        res.on('data', function (chunkBuffer) {
            thirdRequestDataString += chunkBuffer.toString();
        });

        // On response end, break the report data into a groups of one array per line, ignore the first 3 as they are the report name and column names
        res.on('end', function () {
            var splitLinesArray = thirdRequestDataString.split('\n');
            var removedTopLines = splitLinesArray.splice(4, splitLinesArray.length);
            var tempHolder = [];
            var loanData = [];
            //For each line split it into an array with each value being the column data.  Grab the loan number and put into a tempHolder array.
            for (var y = 0; y < removedTopLines.length; y++) {
                var singleLine = removedTopLines[y].split(',');
                loanData.push(singleLine);
                var loanNumber = singleLine[0].replace(/['"]+/g, '');
                tempHolder.push(loanNumber);
            }
            console.log(tempHolder);
            //1a. if nothing on the report end immedately
                //Add code here that returns if tempHolder.length == 0;
            if(tempHolder.length == 0){
                console.log('No new loans found, no work to do');
                return
            }
            //Get percentage needed to maintain and high risk factors
            function getSQLParams() {
                var params = {
                    host: 'cabo.calgudxzrwms.us-east-1.rds.amazonaws.com',
                    user: 'openm',
                    password: '7ACp1G6u0KsXv61N',
                    port: 3306,
                    database: 'web',
                    debug: false,
                    multipleStatements: true
                };
                return mysql.createConnection(params);
            }

            var connection = getSQLParams();
            //modify query to also pull highRisk factors from DB
            var QCQuery = 'SELECT conv, convHighRiskLTV, convHighRiskDTI, convHighRiskCreditScore, convHighRiskPercentage FROM qCData';

            connection.query(QCQuery, function (error, results, fields) {
                if (error) {
                    console.log('Error message:')
                    console.log(error);
                    connection.end();
                } else {
                    console.log('Successful? :')
                    console.log(results);
                    connection.end();
                    //Get high risk factors from the DB and the percentages.
                    var percentageSelected = (results[0].conv) / 100;
                    var highRiskLTV = results[0].convHighRiskLTV;
                    var highRiskDTI = results[0].convHighRiskDTI;
                    var highRiskCreditScore = results[0].convHighRiskCreditScore;
                    var highRiskPercentage = (results[0].convHighRiskPercentage) / 100;
                    console.log(percentageSelected, ' ', highRiskPercentage, ' ' , highRiskLTV ,' ', highRiskDTI ,' ', highRiskCreditScore);

                    //Loop through tempHolder and check for any high risk loans, pull the loan numbers out and put them in their own array.  Otherwise put the loan number in the low risk Loan holder array.
                    var highRiskLoanHolder = [];
                    var lowRiskLoanHolder = [];
                    loanData.forEach( (element,index) =>{
                        if( parseInt(element[5]) >= parseInt(highRiskLTV) || parseInt(element[6]) >= parseInt(highRiskDTI) || element[7] == 'yes' || parseInt(element[9]) < parseInt(highRiskCreditScore) || ( (element[10] == 'refi') && (element[11] == '') ) ){
                            console.log('high risk loan found, adding to high risk array: ', element[0])
                            highRiskLoanHolder.push(element[0]);
                            loanData.splice(index,1)
                        } else{
                            lowRiskLoanHolder.push(element[0]);
                        }
                    });
                    console.log('Low Risk loan holder after check:');
                    console.log(lowRiskLoanHolder);
                    console.log('High Risk loan holder after check:');
                    console.log(highRiskLoanHolder);

                    //Determine outstanding values now and go along with plan.
                    var outstandingValue = Math.ceil(percentageSelected * (totalLoansThisMonth)) - totalFlaggedLoansThisMonth;
                    console.log('Outstanding Value: ', outstandingValue);
                    var highRiskOutstandingValue = Math.ceil(highRiskPercentage * (totalLoansThisMonth)) - totalFlaggedLoansThisMonth;
                    console.log('High Risk Outstanding Value: ', highRiskOutstandingValue);

                    //Scenario 1
                    // If high risk value < or = 0, move all loans to CTC can call it a day, logic here is that the high risk is always greater than the low risk and if the high risk is 0 than no loans of either type need processing.
                    if(highRiskOutstandingValue <= 0){
                        console.log('High Risk threshold not less than or equal to 0, all loans should be moved to CTC');
                        highRiskLoanHolder.forEach(element => {
                            console.log('Scenario 1 high-risk move to CTC for loan: ', element);
                            moveToCTCStatus(element);
                        })
                        lowRiskLoanHolder.forEach(element => {
                            console.log('Scenario 1 low-risk move to CTC for loan: ', element);
                            moveToCTCStatus(element)
                        })
                    }

                    //Scenario 2
                    //  If high Risk value is greater than 0 and low risk is less than/equal to zero, but there are no high risk loans to flag, move all loans to CTC 
                    else if(highRiskOutstandingValue > 0 && outstandingValue <= 0 && highRiskLoanHolder.length <= 0){
                        lowRiskLoanHolder.forEach(element => {
                            console.log('Scenario 2 low-risk move to CTC: ', element);
                            moveToCTCStatus(element)
                        })
                    }

                    //Scenario 3
                    // If high risk value is greater than 0 and low risk is less than/equal to 0, but there ARE high risk loans.  QCFlag the high risk loans, move low to CTC
                    else if(highRiskOutstandingValue > 0 && lowRiskLoanHolder <= 0 && highRiskLoanHolder.length > 0){
                        highRiskLoanHolder.forEach(element => {
                            console.log('Scenario 3 high-risk adding QC flag: ', element);
                            addQcFlag(element)
                        });
                        if(lowRiskLoanHolder.length > 0){
                            lowRiskLoanHolder.forEach(element => {
                                console.log('Scenario 3 low-risk moving to CTC: ', element);
                                moveToCTCStatus(element)
                            })
                        }
                    }

                    //Scenario 4
                    // Both outstanding values are greater than zero but there are no high risk loans in this pass.  Flag low risk as needed.
                    else if(highRiskOutstandingValue >= 0 && outstandingValue >= 0 && highRiskLoanHolder.length <= 0){
                        if(outstandingValue > lowRiskLoanHolder.length){
                            outstandingValue = lowRiskLoanHolder.length;
                        }
                        var scenario4Results = getRandom(lowRiskLoanHolder, outstandingValue);
                        var loansToFlag = scenario4Results[0];
                        var loansToMoveToCTC = scenario4Results[1];

                        loansToFlag.forEach(element => {
                            console.log('Scenario 4 randomly selected low-risk loan adding QC flag to: ', element);
                            addQcFlag(element.slice(1,-1));
                        });

                        loansToMoveToCTC.forEach(element => {
                            console.log('Scenario 4 randomly selected low-risk loan moving to CTC: ', element);
                            moveToCTCStatus(element)
                        });
                    }

                    //Scenario 5
                    //
                    else if(highRiskOutstandingValue >= 0 && outstandingValue >= 0 && highRiskLoanHolder.length >=0 && lowRiskLoanHolder.length >= 0){
                        highRiskLoanHolder.forEach(element => {
                            console.log('Scenario 5 high-risk loan adding QC flag to: ', element);
                            addQcFlag(element);
                            outstandingValue --;
                        });
                        if(outstandingValue > 0){
                            scenario5Results = getRandom(lowRiskLoanHolder, outstandingValue);
                            var loansToFlag = scenario5Results[0];
                            var loansToMoveToCTC = scenario5Results[1];

                            loansToFlag.forEach(element => {
                                console.log('Scenario 5 randomly selected low-risk loan adding QC flag to: ', element);
                                addQcFlag(element);
                            })
                            loansToMoveToCTC.forEach(element =>{
                                console.log('Scenario 5 randomly selected low-risk loan moving to CTC: ', element);
                                moveToCTCStatus(element);
                            })

                        } else{
                            lowRiskLoanHolder.forEach(element => {
                                console.log('Scenario 5 low-risk loan moving to CTC: ', element);
                                moveToCTCStatus(element)
                            });
                        }
                    }
                }
            })
        })

    })
}

//Function to create SOAP Client and modify loans
var addQcFlag = function (loanNumber) {
    console.log('addQcFlag Run!: ', loanNumber);
    var dateObj = new Date();
    var month = dateObj.getMonth() + 1;
    var day = dateObj.getDate();
    var year = dateObj.getFullYear();
    var currentDate = year + '-' + month + '-' + day;
    soap.createClient(lqbAuthUrl, function (err, client) {
        if (err) {
            console.log('err lqbAuth', err);
        }
        console.log('createdClient was successful');
        //Get Auth Ticket and store for use with future actions
        client.GetUserAuthTicket(args, function (err, result) {
            if (err) {
                console.log('Error getting userauthTicket')
            }
            console.log('Got Ticket Okay');
            var ticket = result.GetUserAuthTicketResult;
            console.log(ticket);
            soap.createClient(lqbLoanUrl, function (err, client) {
                if (err) {
                    console.log('err loanCreate Client', err);
                }
                var modifyLoanArgs = {
                    'sTicket': ticket,
                    'sLNm': loanNumber,
                    'sDataContent': `<LOXmlFormat version="1.0">
                        <loan>
                            <field id="sCustomField52D">` + currentDate + `</field>
                        </loan>
                    </LOXmlFormat>`,
                    format: 0
                }
                console.log(modifyLoanArgs);
                client.Save(modifyLoanArgs, function (err, result) {
                    if (err) {
                        console.log('err adding customField52D to loan file', err);
                    } else {
                        console.log('Updated Loan File - added customField52D to loan file');
                        console.log(result);
                    }
                })
            })
        })
    })
};

var moveToCTCStatus = function (loanNumber) {
    console.log('modify Loan Status to CTC Run!: ', loanNumber);
    var dateObj = new Date();
    var month = dateObj.getMonth() + 1;
    var day = dateObj.getDate();
    var year = dateObj.getFullYear();
    var currentDate = year + '-' + month + '-' + day;
    soap.createClient(lqbAuthUrl, function (err, client) {
        if (err) {
            console.log('err lqbAuth', err);
        }
        console.log('createdClient was successful');
        //Get Auth Ticket and store for use with future actions
        client.GetUserAuthTicket(args, function (err, result) {
            if (err) {
                console.log('Error getting userauthTicket')
            }
            console.log('Got Ticket Okay');
            var ticket = result.GetUserAuthTicketResult;
            soap.createClient(lqbLoanUrl, function (err, client) {
                if (err) {
                    console.log('err loanCreate Client', err);
                }
                var modifyLoanArgs = {
                    'sTicket': ticket,
                    'sLNm': loanNumber,
                    'sDataContent': `<LOXmlFormat version="1.0">
                        <loan>
                            <field id="sStatusT">` + '21' + `</field>
                            <field id="sClearToCloseD">` + currentDate + `</field>
                        </loan>
                    </LOXmlFormat>`,
                    format: 0
                }
                console.log(modifyLoanArgs);
                client.Save(modifyLoanArgs, function (err, result) {
                    if (err) {
                        console.log('err updating loan file', err);
                    } else {
                        console.log('Updated Loan File');
                        console.log(result);
                    }
                })
            })
        })
    })
};

var getRandom = function (arr, n) {
    var counter = n;
    if (counter == 0) {
        console.log('counter was 0');
        return [[], arr]
    }
    var clone = arr.slice(0);
    var flaggedArray = [];
    console.log(clone);

    while (counter > 0) {
        var randomIndex = Math.floor(Math.random() * clone.length);
        flaggedArray.push(clone[randomIndex]);
        clone.splice(randomIndex, 1);
        counter--;
    }
    console.log('Clone: ', clone);
    console.log('Flagged Array: ', flaggedArray);
    return [flaggedArray, clone];
};

totalDataFunction(flaggedDataFunction);