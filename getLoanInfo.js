var soap = require('soap');
var http = require('http');
var mysql = require('mysql');
var parseString = require('xml2js').parseString;
var AWS = require('aws-sdk');
AWS.config.loadFromPath('awsconfig.json');
//var ses = new AWS.SES();
var nodemailer = require('nodemailer');
var fs = require('fs');
var PDFDocument = require('pdfkit');
const Stream = require('./stream');


let transporter = nodemailer.createTransport({
    SES: new AWS.SES({
        apiVersion: '2010-12-01'
    })
});

//Constant Variables provided by LQB
var lqbLoanUrl = 'https://webservices.lendingqb.com//los/webservice/Loan.asmx?WSDL';
var args = { "userName": 'cabo', "passWord": 'group61]trim' };
var lqbAuthUrl = 'https://webservices.lendingqb.com/los/webservice/AuthService.asmx?WSDL';
var lqbEDocsUrl = 'https://edocs.lendingqb.com/los/webservice/EDocsService.asmx?WSDL';


soap.createClient(lqbAuthUrl, function (err, client) {
    if (err) {
        console.log('err lqbAuth', err);
    }
    console.log('createdClient was successful');
    //Get Auth Ticket and store for use with future actions
    client.GetUserAuthTicket(args, function (err, result) {
        if (err) {
            console.log('Error getting userauthTicket')
        }
        console.log('Got Ticket Okay');
        var ticket = result.GetUserAuthTicketResult;
        console.log(ticket);
        soap.createClient(lqbLoanUrl, function (err, client) {
            if (err) {
                console.log('err loanCreate Client', err);
            }
            //var loanNumber = '0616x19040090'; // Open Mortgage Logo
            //var loanNumber = '0653x19050194'; // DBA Name
            //var loanNumber = '0499x19040410';  //Demo Example ??
            var loanNumber = '1088x19050530'; //Toy with attaching file to

            var modifyLoanArgs = {
                'sTicket': ticket,
                'sLNm': loanNumber,
                'sXmlQuery': `<loan>
                                <applicant>
                                <field id="aBFirstNm"/>
                                <field id="aBLastNm"/>
                                <field id="aBAddr"/>
                                <field id="aBCity"/>
                                <field id="aBState"/>
                                <field id="aBZip"/>
                                <field id="aCFirstNm"/>
                                <field id="aCLastNm"/>

                                </applicant>
                                <field id="sApprovD"/>
                                <field id="sProd3rdPartyUwResultT"/>
                                <field id="sAppExpD"/>
                                <field id="sDuCaseId"/>
                                <field id="sRLckdExpiredD"/>

                                <field id="sLamtCalc"/>
                                <field id="sFinalLAmt"/>
                                <field id="sProdConvMIOptionT"/>
                                <field id="sFfUfmipFinanced"/>
                                <field id="sNoteIR"/>
                                <field id="sMaxDti"/>
                                <field id="sMaxR"/>
                                <field id="sLtvR"/>
                                <field id="sCltvR"/>
                                <field id="sHcltvR"/>

                                <field id="sLPurposeT"/>
                                <field id="sProdCashoutAmt"/>
                                <field id="sProdImpound"/>
                                <field id="sTerm"/>
                                <field id="sDue"/>
                                <field id="sLpTemplateNm"/>
                                <field id="sServicingPmtT"/>
                                <field id="sDocumentationT"/>

                                <field id="sHouseVal"/>
                                <field id="sApprVal"/>
                                <field id="sApprRprtExpD"/>
                                <field id="sProdSpT"/>
                                <field id="sOccT"/>
                                <field id="sAssetExpD"/>

                                <field id="sLTotI"/>
                                <field id="sIncomeDocExpD"/>
                                <field id="sProThisMPmt"/>
                                <field id="sQualTopR"/>
                                <field id="sQualBottomR"/>

                                <field id="sCreditScoreType2"/>
                                <field id="sCrExpD"/>

                                <field id="sPrelimRprtDocumentD"/>
                                <field id="sPrelimRprtExpD"/>
                                                                
                                <field id="sEmployeeLoanRepName"/>
                                <field id="sEmployeeLoanRepEmail"/>
                                <field id="sEmployeeLoanRepPhone"/>
                                <field id="sEmployeeProcessorName"/>
                                <field id="sEmployeeProcessorEmail"/>
                                <field id="sEmployeeProcessorPhone"/>
                                <field id="sEmployeeUnderwriterName"/>
                                <field id="sEmployeeUnderwriterEmail"/>
                                <field id="sEmployeeUnderwriterPhone"/>
                                <field id="sTRIDMortgageBrokerName"/>
                                <field id="sBranchName"/>
                                <field id="sSpAddr"/>
                                <field id="sSpCity"/>
                                <field id="sSpState"/>
                                <field id="sSpZip"/>
                                <field id="sEmployeeBrokerProcessorName"/>
                                <field id="sEmployeeBrokerProcessorEmail"/>
                                <field id="sEmployeeBrokerProcessorPhone"/>
                                <field id="sEmployeeLoanRepFax"/>
                                <field id="sEmployeeProcessorFax"/>
                                <field id="sEmployeeUnderwriterFax"/>
                                <field id="sFreddieLoanId"/>
                                <field id="sIsBranchActAsOriginatorForFileTri"/>
                                <field id="sBranchChannelT"/>
                                <field id="sEmployeeLoanOfficerAssistantEmail"/>
                                <collection id="sCondDataSet"/>
                                
                                
                            </loan>`,
                format: 0
            }
            console.log('Attempting to Load Loan items now:');
            client.Load(modifyLoanArgs, function (err, result) {
                if (err) {
                    console.log(client.lastRequest);
                    console.log('err getting conditions on loan file:');
                    console.log(err.body);
                } else {
                    console.log('Contents of conditionRequest Body:');
                    //console.log(result);
                    var xml = result.LoadResult;
                    parseString(xml, function (err, result) {
                        //console.log(result.LOXmlFormat.loan[0].collection[0].record);
                        var conditionsHolder = [];
                        var conditionsInfo = result.LOXmlFormat.loan[0].collection[0].record;
                        //console.log(conditionsInfo[68].field[4]['_']);
                        for (var x = 0; x < conditionsInfo.length; x++) {
                            //console.log(conditionsInfo[x].field[11]['_']);
                            if (conditionsInfo[x].field[11]['_'] != 'Closed') {
                                //console.log(conditionsInfo[x].field);
                                var conditionNumber = conditionsInfo[x].field[4]['_'];
                                var message = conditionsInfo[x].field[3]['_'];
                                var status = conditionsInfo[x].field[2]['_'];
                                conditionsHolder.push({ conditionNumber: conditionNumber, message: message, status: status })
                            }
                        }
                        //console.log(conditionsInfo.length);
                        //console.log(conditionsHolder);
                        //console.log(result.LOXmlFormat.loan[0].field[0]['_']);
                        var informationHolder = {};
                        var borrowerInfo = result.LOXmlFormat.loan[0].applicant[0].field;

                        var borrowerFirstName = borrowerInfo[0]['_'];
                        informationHolder.borrowerFirstName = borrowerFirstName;
                        var borrowerLastName = borrowerInfo[1]['_'];
                        informationHolder.borrowerLastName = borrowerLastName;
                        var borrowerAddr = borrowerInfo[2]['_'];
                        informationHolder.borrowerAddr = borrowerAddr;
                        var borrowerCity = borrowerInfo[3]['_'];
                        informationHolder.borrowerCity = borrowerCity;
                        var borrowerState = borrowerInfo[4]['_'];
                        informationHolder.borrowerState = borrowerState;
                        var borrowerZip = borrowerInfo[5]['_'];
                        informationHolder.borrowerZip = borrowerZip;
                        var coBorrowerFirstName = borrowerInfo[6]['_'];
                        informationHolder.coBorrowerFirstName = coBorrowerFirstName;
                        var coBorrowerLastName = borrowerInfo[7]['_'];
                        informationHolder.coBorrowerLastName = coBorrowerLastName;
                        console.log(borrowerInfo);


                        var result = result.LOXmlFormat.loan[0];
                        console.log(result);

                        var approvalDate = result.field[0]['_'];
                        informationHolder.approvalDate = approvalDate;
                        var auResponse = result.field[1]['_'];
                        if (auResponse == 1) {
                            auResponse = 'DU Approve/Eligible';
                        } else if (auResponse == 2) {
                            auResponse = 'DU Approve/Ineligible';
                        } else if (auResponse == 3) {
                            auResponse = 'DU Refer/Eligible';
                        } else if (auResponse == 4) {
                            auResponse = 'DU Refer/Ineligible';
                        } else if (auResponse == 5) {
                            auResponse = 'Refer/With Caution Eligible';
                        } else if (auResponse == 6) {
                            auResponse = 'Refer/With Caution Ineligible';
                        } else if (auResponse == 7) {
                            auResponse = 'DU EAI/Eligible';
                        } else if (auResponse == 8) {
                            auResponse = 'DU EAII/Eligible';
                        } else if (auResponse == 9) {
                            auResponse = 'DU EAIII/Eligible';
                        } else if (auResponse == 10) {
                            auResponse = 'LP Accept/Eligible';
                        } else if (auResponse == 11) {
                            auResponse = 'LP Accept/Ineligible';
                        } else if (auResponse == 12) {
                            auResponse = 'LP Caution/Eligible';
                        } else if (auResponse == 13) {
                            auResponse = 'LP Caution/Ineligible';
                        } else if (auResponse == 14) {
                            auResponse = 'Out of Scope'
                        } else if (auResponse == 15) {
                            auResponse = 'LP AMinus Level 1';
                        } else if (auResponse == 16) {
                            auResponse = 'LP AMinus Level 2';
                        } else if (auResponse == 17) {
                            auResponse = 'LP AMinus Level 3';
                        } else if (auResponse == 18) {
                            auResponse = 'LP AMinus Level 4';
                        } else if (auResponse == 19) {
                            auResponse = 'LP AMinus Level 5';
                        } else if (auResponse == 20) {
                            auResponse = 'LP Refer';
                        } else if (auResponse == 21) {
                            auResponse = 'Total Approve/Eligible';
                        } else if (auResponse == 22) {
                            auResponse = 'Total Approve/Ineligible';
                        } else if (auResponse == 23) {
                            auResponse = 'Total Refer/Eligible';
                        } else if (auResponse == 24) {
                            auResponse = 'Total Refer/Ineligible';
                        } else if (auResponse == 25) {
                            auResponse = 'GUS Accept/Eligible';
                        } else if (auResponse == 26) {
                            auResponse = 'GUS Accept/Ineligible';
                        } else if (auResponse == 27) {
                            auResponse = 'GUS Refer/Eligible';
                        } else if (auResponse == 28) {
                            auResponse = 'GUS Refer/Ineligible';
                        } else if (auResponse == 29) {
                            auResponse = 'GUS Refer/With Caution Eligible';
                        } else if (auResponse == 30) {
                            auResponse = 'GUS Refer/With Caution Ineligible';
                        } else if (auResponse == 0) {
                            auResponse = 'NA';
                        }
                        informationHolder.auResponse = auResponse
                        var approvalExpDate = result.field[2]['_'];
                        informationHolder.approvalExpDate = approvalExpDate
                        var duCaseId = result.field[3]['_'];
                        informationHolder.duCaseId = duCaseId;
                        var rateLockExp = result.field[4]['_'];
                        informationHolder.rateLockExp = rateLockExp;


                        var loanAmount = result.field[5]['_'];
                        informationHolder.loanAmount = loanAmount;
                        var finalLoanAmount = result.field[6]['_'];
                        informationHolder.finalLoanAmount = finalLoanAmount;
                        var mortgageIns = result.field[7]['_'];
                        if (mortgageIns == 0) {
                            mortgageIns = 'Borrower Paid Monthly Prem';
                        } else if (mortgageIns == 1) {
                            mortgageIns = 'Borrower Paid Single Prem';
                        } else if (mortgageIns == 2) {
                            mortgageIns = 'Borrower Paid Split Prem';
                        } else if (mortgageIns == 3) {
                            mortgageIns = 'Lead Paid Single Prem';
                        } else if (mortgageIns == 4) {
                            mortgageIns = 'No MI';
                        } else if (mortgageIns == 5) {
                            mortgageIns = 'Blank';
                        }
                        informationHolder.mortgageIns = mortgageIns;
                        var upfrontMIPFee = result.field[8]['_'];
                        informationHolder.upfrontMIPFee = upfrontMIPFee;
                        var noteRate = result.field[9]['_'];
                        informationHolder.noteRate = noteRate;
                        var maxDti = result.field[10]['_'];
                        informationHolder.maxDti = maxDti;
                        var dtiRate = result.field[11]['_'];
                        informationHolder.dtiRate = dtiRate;
                        var ltv = result.field[12]['_'];
                        informationHolder.ltv = ltv;
                        var cltv = result.field[13]['_'];
                        informationHolder.cltv = cltv;
                        var hcltv = result.field[14]['_'];
                        informationHolder.hcltv = hcltv;

                        var loanPurpose = result.field[15]['_'];
                        if (loanPurpose == 0) {
                            loanPurpose = 'Purchase';
                        } else if (loanPurpose == 1) {
                            loanPurpose = 'Refinance';
                        } else if (loanPurpose == 2) {
                            loanPurpose = 'Refinance Cash Out';
                        } else if (loanPurpose == 3) {
                            loanPurpose = 'Construction';
                        } else if (loanPurpose == 4) {
                            loanPurpose = 'Construction Perm';
                        } else if (loanPurpose == 5) {
                            loanPurpose = 'Other';
                        } else if (loanPurpose == 6) {
                            loanPurpose = 'FHA Streamlined Refinance';
                        } else if (loanPurpose == 7) {
                            loanPurpose = 'Valrrrl';
                        } else if (loanPurpose == 8) {
                            loanPurpose = 'Home Equity';
                        }
                        informationHolder.loanPurpose = loanPurpose;
                        var cashOutAmount = result.field[16]['_'];
                        informationHolder.cashOutAmount = cashOutAmount;
                        var impound = result.field[17]['_'];
                        if (impound == 'False') {
                            impound = 'No'
                        } else {
                            impound = 'Yes'
                        }
                        informationHolder.impound = impound;
                        var term = result.field[18]['_'];
                        informationHolder.term = term;
                        var due = result.field[19]['_'];
                        informationHolder.due = due;
                        var loanProgram = result.field[20]['_'];
                        informationHolder.loanProgram = loanProgram;
                        var paymentType = result.field[21]['_'];
                        paymentType = 'Principal & Interest';
                        informationHolder.paymentType = 'Principal & Interest';
                        var documentation = result.field[22]['_'];
                        if (documentation == 0) {
                            documentation = 'Full Document';
                        } else if (documentation == 1) {
                            documentation = 'Alt Document';
                        }
                        informationHolder.documentation = documentation;

                        var homeValue = result.field[23]['_'];
                        informationHolder.homeValue = homeValue;
                        var appValue = result.field[24]['_'];
                        informationHolder.appValue = appValue;
                        var AppExp = result.field[25]['_'];
                        informationHolder.AppExp = AppExp;
                        var propertyType = result.field[26]['_'];
                        if (propertyType == 0) {
                            propertyType = 'SFR';
                        } else if (propertyType == 1) {
                            propertyType = 'PUD';
                        } else if (propertyType == 2) {
                            propertyType = 'Condo';
                        } else if (propertyType == 3) {
                            propertyType = 'CoOp';
                        } else if (propertyType == 4) {
                            propertyType = 'Manufactured';
                        } else if (propertyType == 5) {
                            propertyType = 'Townhouse';
                        } else if (propertyType == 6) {
                            propertyType = 'Commercial';
                        } else if (propertyType == 7) {
                            propertyType = 'Mixed Use';
                        } else if (propertyType == 8) {
                            propertyType = 'Two Unit';
                        } else if (propertyType == 9) {
                            propertyType = 'Three Unit';
                        } else if (propertyType == 10) {
                            propertyType = 'Four Unit';
                        } else if (propertyType == 11) {
                            propertyType = 'Modular';
                        } else if (propertyType == 12) {
                            propertyType = 'Rowhouse';
                        }
                        informationHolder.propertyType = propertyType;
                        var propertyPurpose = result.field[27]['_'];
                        if (propertyPurpose == 0) {
                            propertyPurpose = 'Primary Residence';
                        } else if (propertyPurpose == 1) {
                            propertyPurpose = 'Secondary Residence';
                        } else if (propertyPurpose == 2) {
                            propertyPurpose = 'Investment';
                        }
                        informationHolder.propertyPurpose = propertyPurpose;
                        var assetExp = result.field[28]['_'];
                        informationHolder.assetExp = assetExp;

                        var totalIncome = result.field[29]['_'];
                        informationHolder.totalIncome = totalIncome;
                        var incomeDocExp = result.field[30]['_'];
                        informationHolder.incomeDocExp = incomeDocExp;
                        var pAndI = result.field[31]['_'];
                        informationHolder.pAndI = pAndI;
                        var ratioTop = result.field[32]['_'];
                        informationHolder.ratioTop = ratioTop;
                        var ratioBottom = result.field[33]['_'];
                        informationHolder.ratioBottom = ratioBottom;

                        var creditScore = result.field[34]['_'];
                        informationHolder.creditScore = creditScore;
                        var creditExp = result.field[35]['_'];
                        informationHolder.creditExp = creditExp;
                        var prelimTitleDocDate = result.field[36]['_'];
                        informationHolder.prelimTitleDocDate = prelimTitleDocDate;
                        var prelimTitleExp = result.field[37]['_'];
                        informationHolder.prelimTitleExp = prelimTitleExp;

                        var loName = result.field[38]['_'];
                        informationHolder.loName = loName;
                        var loEmail = result.field[39]['_'];
                        informationHolder.loEmail = loEmail;
                        var loPhone = result.field[40]['_'];
                        informationHolder.loPhone = loPhone;
                        var processorName = result.field[41]['_'];
                        informationHolder.processorName = processorName;
                        var processorEmail = result.field[42]['_'];
                        informationHolder.processorEmail = processorEmail;
                        var processorPhone = result.field[43]['_'];
                        informationHolder.processorPhone = processorPhone;
                        var uwName = result.field[44]['_'];
                        informationHolder.uwName = uwName;
                        var uwEmail = result.field[45]['_'];
                        informationHolder.uwEmail = uwEmail;
                        var uwPhone = result.field[46]['_'];
                        informationHolder.uwPhone = uwPhone;
                        var displayBranchName = result.field[47]['_'];
                        informationHolder.displayBranchName = displayBranchName;
                        var branchName = result.field[48]['_'];
                        informationHolder.branchName = branchName;
                        if (displayBranchName && displayBranchName != "") {
                            branchName = displayBranchName;
                        } else {

                        }
                        var propAdd = result.field[49]['_'];
                        informationHolder.propAdd = propAdd;
                        var propCity = result.field[50]['_'];
                        informationHolder.propCity = propCity;
                        var propState = result.field[51]['_'];
                        informationHolder.propState = propState;
                        var propZip = result.field[52]['_'];
                        informationHolder.propZip = propZip;
                        var extProcName = result.field[53]['_'];
                        informationHolder.extProcName = extProcName;
                        if (!processorName && extProcName != "") {
                            processorName = extProcName
                        }
                        var extProcEmail = result.field[54]['_'];
                        informationHolder.extProcEmail = extProcEmail;
                        if (!processorEmail && extProcEmail != "") {
                            processorEmail = extProcEmail
                        }
                        var extProcPhone = result.field[55]['_'];
                        informationHolder.extProcPhone = extProcPhone;
                        if (!processorPhone && extProcPhone != "") {
                            processorPhone = extProcPhone
                        }
                        var loFax = result.field[56]['_'];
                        informationHolder.loFax = loFax;
                        var processorFax = result.field[57]['_'];
                        informationHolder.processorFax = processorFax;
                        var uwFax = result.field[58]['_'];
                        informationHolder.uwFax = uwFax;
                        var lpNum = result.field[59]['_'];
                        informationHolder.lpNum = lpNum;
                        var openBool = result.field[60]['_'];
                        informationHolder.openBool = openBool;
                        var loanChannel = result.field[61]['_'];
                        informationHolder.loanChannel = loanChannel;
                        var loaEmail = result.field[62]['_'];
                        informationHolder.loaEmail = loaEmail;
                        //console.log(informationHolder);


                        let pdf = new PDFDocument();
                        let buffers = [];


                        pdf.on('data', buffers.push.bind(buffers));
                        pdf.on('end', () => {
                            let pdfData = Buffer.concat(buffers);
                            var baseString = pdfData.toString('base64');
                            //console.log(pdfData.toString('base64'));
                            var mailList = [processorEmail, loEmail, loaEmail, 'davidmilco@corp.openmtg.com'];
                            //var mailList = [processorEmail, loEmail, 'davidmilco@corp.openmtg.com', loaEmail]
                            transporter.sendMail({
                                from: 'QCAutomation@openmortgage.com',
                                to: mailList,
                                //to: 'davidmilco@openmortgage.com',
                                subject: 'Approval Certificate for Loan#: ' + loanNumber + ' ' + borrowerLastName,
                                text: 'Attached is the Approval Certificate for the following loan: ' + loanNumber + '. <br><br>It has been attached to the Edocs of the loan file.',
                                attachments: [
                                    {
                                        filename: "approvalCertificate_" + loanNumber + ".pdf",
                                        content: pdfData
                                    }
                                ],
                                ses: { // optional extra arguments for SendRawEmail
                                    Tags: [{
                                        Name: 'tagName',
                                        Value: 'tagValue'
                                    }]
                                }
                            }, (err, info) => {
                                if (err) {
                                    console.log('Error');
                                    console.log(err)
                                } else {
                                    console.log(info);
                                    var uploadPdfArgs = {
                                        'sTicket': ticket,
                                        'sLNm': loanNumber,
                                        'documentType': 'APPROVAL CERTIFICATE',
                                        'notes': '',
                                        'sDataContent': baseString,
                                    }
                                    soap.createClient(lqbEDocsUrl, function (err, eDocsClient) {
                                        if (err) {
                                            console.log('Error creating Edocs Client: ', err);

                                        } else {
                                            console.log('Successfully created Edocs Client');
                                            eDocsClient.UploadPDFDocument(uploadPdfArgs, function (err, result) {
                                                if (err) {
                                                    console.log('Error in uploadPDFDocument: ', err);

                                                } else {
                                                    console.log('All the way through the logic, so hopefully it did what it needed to do and sent the email and attached the doc, now to change the customfield40 date');
                                                    if (loanChannel == "2" || loanChannel == "3" || loanChannel == "4") {
                                                        var dateObj = new Date();
                                                        var month = dateObj.getMonth() + 1;
                                                        var day = dateObj.getDate();
                                                        var year = dateObj.getFullYear();
                                                        var currentDate = year + '-' + month + '-' + day;
                                                        var modifyLoanArgs = {
                                                            'sTicket': ticket,
                                                            'sLNm': loanNumber,
                                                            'sDataContent': `<LOXmlFormat version="1.0">
                                                            <loan>
                                                                <field id="sCustomField40D">` + currentDate + `</field>
                                                            </loan>
                                                        </LOXmlFormat>`,
                                                            format: 0
                                                        };
                                                        client.Save(modifyLoanArgs, function (err, result) {
                                                            if (err) {
                                                                console.log('Error in saving customDate40:');
                                                                console.log(err)
                                                            } else {
                                                                console.log('Logic finished, the date should be in and updated.')
                                                            }
                                                        })
                                                    } else {
                                                        console.log('Loan is not wholesale you are done!');
                                                    }
                                                }
                                            })
                                        }
                                    })
                                }
                            });
                        })
                        pdf.fontSize(12);
                        if (openBool != '1')
                            pdf.font('Times-Bold').text(branchName, {
                                align: 'right'
                            }); else if (openBool == '1') {
                                pdf.image('Logo_OM_Blue_050517.png', 400, 20, { width: 150, align: 'right' });
                            }
                        pdf.fontSize(14);
                        pdf.font('Times-Bold').text('APPROVAL CERTIFICATE', {
                            align: 'center'
                        });
                        pdf.fontSize(9);
                        var d = new Date();
                        var month = new Array();
                        month[0] = "January";
                        month[1] = "February";
                        month[2] = "March";
                        month[3] = "April";
                        month[4] = "May";
                        month[5] = "June";
                        month[6] = "July";
                        month[7] = "August";
                        month[8] = "September";
                        month[9] = "October";
                        month[10] = "November";
                        month[11] = "December";
                        var currentMonth = month[d.getMonth()];
                        var currentDay = d.getDate().toString();
                        var currentYear = d.getFullYear().toString();
                        var currentMinutes = d.getMinutes().toString();
                        if (currentMinutes.length < 2) {
                            currentMinutes = '0' + currentMinutes
                        }
                        var currentTime = (d.getHours() + ':' + currentMinutes).toString();
                        pdf.font('Times-Bold').text(currentMonth + ' ' + currentDay + ', ' + currentYear + ' ' + currentTime + ' CST', {
                            align: 'left'
                        });
                        pdf.font('Times-Roman').text('Open Mortgage, LLC')
                        pdf.text('14101 W Hwy 290, Building 1300')
                        pdf.text('Austin, TX 78737')
                        pdf.text('This mortgage loan has been approved on behalf of the above noted client with the following terms and conditions.')
                        if (openBool != "1") {
                            pdf.font('Times-Bold').text('Ref #: ' + loanNumber, 300, 102)
                            var borrowerString = borrowerFirstName + ' ' + borrowerLastName;
                            console.log(coBorrowerFirstName);
                            if (coBorrowerFirstName != '' || coBorrowerFirstName != undefined) {
                                console.log('Does this run?');
                                borrowerString = borrowerString + ' & ' + coBorrowerFirstName + ' ' + coBorrowerLastName;
                            }

                            pdf.font('Times-Roman').text(borrowerString, 300, 111)
                            pdf.text(propAdd, 300, 122)
                            pdf.text(propCity + ', ' + propState + ' ' + propZip, 300, 131)

                        } else if (openBool == "1") {
                            pdf.font('Times-Bold').text('Ref #: ' + loanNumber, 300, 87)
                            var borrowerString = borrowerFirstName + ' ' + borrowerLastName;
                            if (coBorrowerFirstName != '') {
                                borrowerString = borrowerString + ' & ' + coBorrowerFirstName + ' ' + coBorrowerLastName;
                            }
                            pdf.font('Times-Roman').text(borrowerString, 300, 97)
                            pdf.text(propAdd, 300, 107)
                            pdf.text(propCity + ', ' + propState + ' ' + propZip, 300, 117)
                        }
                        pdf.rect(75, 155, 460, 1).stroke();

                        pdf.fontSize(9);
                        pdf.font('Times-Bold').text('Loan Number', 75, 160)
                        pdf.font('Times-Bold').text('ApprovalDate', 75, 170)
                        if (duCaseId) {
                            pdf.font('Times-Bold').text('DO/DU Case ID', 75, 180)
                        } else if (lpNum) {
                            pdf.font('Times-Bold').text('LP Loan ID', 75, 180)
                        }
                        pdf.font('Times-Roman').text(loanNumber, 175, 160)
                        pdf.font('Times-Roman').text(approvalDate, 175, 170)
                        if (duCaseId) {
                            pdf.font('Times-Roman').text(duCaseId, 175, 180)
                        } else if (lpNum) {
                            pdf.font('Times-Roman').text(lpNum, 175, 180)
                        }


                        pdf.font('Times-Bold').text('AU Response', 300, 160)
                        pdf.font('Times-Bold').text('Approval Exp Date', 300, 170)
                        pdf.font('Times-Bold').text('Rate Lock Expiration', 300, 180)
                        pdf.font('Times-Roman').text(auResponse, 400, 160)
                        pdf.font('Times-Roman').text(approvalExpDate, 400, 170)
                        pdf.font('Times-Roman').text(rateLockExp, 400, 180)

                        pdf.rect(75, 200, 460, 1).stroke();

                        pdf.font('Times-Bold').text('Loan Information', 75, 210, { align: 'center', underline: true })
                        pdf.font('Times-Bold').text('Loan Amount', 75, 220)
                        pdf.font('Times-Bold').text('Total Loan Amount', 75, 230)
                        pdf.font('Times-Bold').text('Mortgage Insurance', 75, 240)
                        pdf.font('Times-Bold').text('Upfront MIP Fee', 75, 250)
                        pdf.font('Times-Bold').text('Financed', 75, 260)
                        pdf.font('Times-Bold').text('Note Rate', 75, 270)
                        pdf.font('Times-Bold').text('Max DTI / Rate', 75, 280)
                        pdf.font('Times-Bold').text('LTV / CLTV / HCLTV', 75, 290)
                        pdf.font('Times-Roman').text(loanAmount, 175, 220)
                        pdf.font('Times-Roman').text(finalLoanAmount, 175, 230)
                        pdf.font('Times-Roman').text(mortgageIns, 175, 240)
                        pdf.font('Times-Roman').text(upfrontMIPFee, 175, 250)
                        pdf.font('Times-Roman').text(noteRate, 175, 270)
                        pdf.font('Times-Roman').text(maxDti + '% / ' + dtiRate + '%', 175, 280)
                        pdf.font('Times-Roman').text(ltv + ' / ' + cltv + ' / ' + hcltv, 175, 290)

                        pdf.font('Times-Bold').text('Loan Purpose', 300, 220)
                        pdf.font('Times-Bold').text('Cashout Amount', 300, 230)
                        pdf.font('Times-Bold').text('Impound?', 300, 240)
                        pdf.font('Times-Bold').text('Term / Due', 300, 250)
                        pdf.font('Times-Bold').text('Loan Program', 300, 260)
                        pdf.font('Times-Bold').text('Payment Type', 300, 280)
                        pdf.font('Times-Bold').text('Documentation', 300, 290)
                        pdf.font('Times-Roman').text(loanPurpose, 400, 220)
                        pdf.font('Times-Roman').text(cashOutAmount, 400, 230)
                        pdf.font('Times-Roman').text(impound, 400, 240)
                        pdf.font('Times-Roman').text(term + ' / ' + due + ' Months', 400, 250)
                        pdf.font('Times-Roman').text(loanProgram, 400, 260)
                        pdf.font('Times-Roman').text(paymentType, 400, 280)
                        pdf.font('Times-Roman').text(documentation, 400, 290)



                        pdf.rect(75, 310, 460, 1).stroke();

                        pdf.font('Times-Bold').text('Collateral', 75, 320, { align: 'center', underline: true })
                        pdf.font('Times-Bold').text('Home Value', 75, 330)
                        pdf.font('Times-Bold').text('Appraised Value', 75, 340)
                        pdf.font('Times-Bold').text('Appraisial Exp Date', 75, 350)
                        pdf.font('Times-Roman').text(homeValue, 175, 330)
                        pdf.font('Times-Roman').text(appValue, 175, 340)
                        pdf.font('Times-Roman').text(AppExp, 175, 350)

                        pdf.font('Times-Bold').text('Property Type', 300, 330)
                        pdf.font('Times-Bold').text('Property Purpose', 300, 340)
                        pdf.font('Times-Bold').text('Asset Exp Date', 300, 350)
                        pdf.font('Times-Roman').text(propertyType, 400, 330)
                        pdf.font('Times-Roman').text(propertyPurpose, 400, 340)
                        pdf.font('Times-Roman').text(assetExp, 400, 350)

                        pdf.rect(75, 370, 460, 1).stroke();

                        pdf.font('Times-Bold').text('Income', 75, 380, { align: 'center', underline: true })
                        pdf.font('Times-Bold').text('Monthly Income', 75, 390)
                        pdf.font('Times-Bold').text('Income Doc Exp Date', 75, 400)
                        pdf.font('Times-Roman').text(totalIncome, 175, 390)
                        pdf.font('Times-Roman').text(incomeDocExp, 175, 400)
                        pdf.font('Times-Bold').text('Principal & Interest', 300, 390)
                        pdf.font('Times-Bold').text('Ratios', 300, 400)
                        pdf.font('Times-Roman').text(pAndI, 400, 390)
                        pdf.font('Times-Roman').text(ratioTop + ' / ' + ratioBottom, 400, 400)

                        pdf.rect(75, 420, 460, 1).stroke();

                        pdf.font('Times-Bold').text('Credit', 75, 430, { align: 'center', underline: true })
                        pdf.font('Times-Bold').text('Qualifying Score', 75, 440)
                        pdf.font('Times-Roman').text(creditScore, 175, 440)
                        pdf.font('Times-Bold').text('Credit Exp Date', 300, 440)
                        pdf.font('Times-Roman').text(creditExp, 400, 440)

                        pdf.rect(75, 460, 460, 1).stroke();

                        pdf.font('Times-Bold').text('Preliminary Title Report', 75, 470, { align: 'center', underline: true })
                        pdf.font('Times-Bold').text('Preliminary Doc Date', 75, 480)
                        pdf.font('Times-Roman').text(prelimTitleDocDate, 175, 480)
                        pdf.font('Times-Bold').text('Prelim Title Exp Date', 300, 480)
                        pdf.font('Times-Roman').text(prelimTitleExp, 400, 480)

                        pdf.rect(75, 500, 460, 1).stroke();

                        pdf.font('Times-Bold').text('Loan Officer', 75, 510, { align: 'center', underline: true })
                        pdf.font('Times-Bold').text('Contact', 75, 520)
                        pdf.font('Times-Bold').text('E-mail', 75, 530)
                        pdf.font('Times-Roman').text(loName, 125, 520)
                        pdf.font('Times-Roman').text(loEmail, 125, 530, { width: 170 })
                        pdf.font('Times-Bold').text('Phone', 300, 520)
                        pdf.font('Times-Bold').text('Fax', 300, 530)
                        pdf.font('Times-Roman').text(loPhone, 350, 520)
                        pdf.font('Times-Roman').text(loFax, 350, 530)

                        pdf.rect(75, 550, 460, 1).stroke();

                        pdf.font('Times-Bold').text('Processor', 75, 560, { align: 'center', underline: true })
                        pdf.font('Times-Bold').text('Contact', 75, 570)
                        pdf.font('Times-Bold').text('E-mail', 75, 580)
                        pdf.font('Times-Roman').text(processorName, 125, 570)
                        pdf.font('Times-Roman').text(processorEmail, 125, 580, { width: 170 })
                        pdf.font('Times-Bold').text('Phone', 300, 570)
                        pdf.font('Times-Bold').text('Fax', 300, 580)
                        pdf.font('Times-Roman').text(processorPhone, 350, 570)
                        pdf.font('Times-Roman').text(processorFax, 350, 580)

                        pdf.rect(75, 600, 460, 1).stroke();

                        pdf.font('Times-Bold').text('Underwriter', 75, 610, { align: 'center', underline: true })
                        pdf.font('Times-Bold').text('Contact', 75, 620)
                        pdf.font('Times-Bold').text('E-mail', 75, 630)
                        pdf.font('Times-Roman').text(uwName, 125, 620)
                        pdf.font('Times-Roman').text(uwEmail, 125, 630, { width: 170 })
                        pdf.font('Times-Bold').text('Phone', 300, 620)
                        pdf.font('Times-Bold').text('Fax', 300, 630)
                        pdf.font('Times-Roman').text(uwPhone, 350, 620)
                        pdf.font('Times-Roman').text(uwFax, 350, 630)

                        pdf.rect(75, 650, 460, 1).stroke();


                        pdf.font('Times-Bold').text('Underwriter Signature:', 75, 670)
                        pdf.rect(170, 675, 150, 1).stroke();

                        pdf.font('Times-Bold').text('Date:', 450, 670)
                        pdf.rect(475, 675, 75, 1).stroke();

                        pdf.addPage();
                        pdf.fontSize(14);
                        pdf.font('Times-Bold').text('APPROVAL CERTIFICATE', {
                            align: 'center'
                        });
                        pdf.fontSize(9);
                        var d = new Date();
                        var month = new Array();
                        month[0] = "January";
                        month[1] = "February";
                        month[2] = "March";
                        month[3] = "April";
                        month[4] = "May";
                        month[5] = "June";
                        month[6] = "July";
                        month[7] = "August";
                        month[8] = "September";
                        month[9] = "October";
                        month[10] = "November";
                        month[11] = "December";
                        pdf.font('Times-Bold').text(currentMonth + ' ' + currentDay + ', ' + currentYear + ' ' + currentTime + ' CST', {
                            align: 'left'
                        });
                        pdf.font('Times-Roman').text('Open Mortgage, LLC')
                        pdf.text('14101 W Hwy 290, Building 1300')
                        pdf.text('Austin, TX 78737')
                        pdf.moveDown();
                        pdf.text('This mortgage loan has been approved on behalf of the above noted client with the following terms and conditions.')
                        pdf.font('Times-Bold').text('Ref #: ' + loanNumber, 300, 88)
                        pdf.font('Times-Roman').text(borrowerFirstName + ' ' + borrowerLastName, 300, 98)
                        pdf.text(propAdd, 300, 108)
                        pdf.text(propCity + ', ' + propState + ' ' + propZip, 300, 118)
                        //pdf.rect(75, 155, 460, 1).stroke();

                        pdf.moveDown();
                        pdf.moveDown();
                        var fundingCond = [];
                        var docsCond = []
                        var miscCond = [];
                        var fundCond = [];
                        var uwReviewCond = [];
                        var discCond = [];
                        var trailingDocs = [];
                        for (var x = 0; x < conditionsHolder.length; x++) {
                            if (conditionsHolder[x].status == "PRIOR TO FUNDING") {
                                fundingCond.push(conditionsHolder[x]);
                            } else if (conditionsHolder[x].status == "PRIOR TO DOCS") {
                                docsCond.push(conditionsHolder[x]);
                            } else if (conditionsHolder[x].status == "MISC") {
                                miscCond.push(conditionsHolder[x]);
                            } else if (conditionsHolder[x].status == "FUND") {
                                fundCond.push(conditionsHolder[x])
                            } else if (conditionsHolder[x].status == "UW TO REVIEW") {
                                uwReviewCond.push(conditionsHolder[x])
                            } else if (conditionsHolder[x].status == "DISCLOSURES") {
                                discCond.push(conditionsHolder[x]);
                            } else if (conditionsHolder[x].status == "TRAILING DOCS") {
                                trailingDocs.push(conditionsHolder[x]);
                            }
                        }

                        if (trailingDocs.length > 0) {
                            var currentLevel = pdf.y;
                            pdf.rect(75, pdf.y, 460, 1).stroke();
                            pdf.moveDown();
                            pdf.moveDown();
                            pdf.font('Times-Bold').text('TRAILING DOCS', 200, currentLevel + 5, { underline: true });
                            pdf.font('Times-Bold').text('Date Cleared', 400, currentLevel + 5);
                            pdf.font('Times-Bold').text('Cleared By', 475, currentLevel + 5);
                            pdf.moveDown();
                            for (x = 0; x < trailingDocs.length; x++) {
                                var loopLevel = pdf.y;
                                pdf.font('Times-Roman').text(trailingDocs[x].conditionNumber, 75, pdf.y, { width: 20 })
                                pdf.font('Times-Roman').text(trailingDocs[x].message, 100, loopLevel, { width: 300 })
                                pdf.rect(400, loopLevel + pdf.heightOfString(trailingDocs[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.rect(475, loopLevel + pdf.heightOfString(trailingDocs[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.moveDown();
                                if (pdf.y > 600) {
                                    pdf.addPage();
                                }
                            }
                        }

                        if (fundingCond.length > 0) {
                            var currentLevel = pdf.y;
                            pdf.rect(75, pdf.y, 460, 1).stroke();
                            pdf.moveDown();
                            pdf.moveDown();
                            pdf.font('Times-Bold').text('PRIOR TO FUNDING', 200, currentLevel + 5, { underline: true });
                            pdf.font('Times-Bold').text('Date Cleared', 400, currentLevel + 5);
                            pdf.font('Times-Bold').text('Cleared By', 475, currentLevel + 5);
                            pdf.moveDown();
                            for (x = 0; x < fundingCond.length; x++) {
                                var loopLevel = pdf.y;
                                pdf.font('Times-Roman').text(fundingCond[x].conditionNumber, 75, pdf.y, { width: 20 })
                                pdf.font('Times-Roman').text(fundingCond[x].message, 100, loopLevel, { width: 300 })
                                pdf.rect(400, loopLevel + pdf.heightOfString(fundingCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.rect(475, loopLevel + pdf.heightOfString(fundingCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.moveDown();
                                if (pdf.y > 600) {
                                    pdf.addPage();
                                }
                            }
                        }

                        if (docsCond.length > 0) {
                            var currentLevel = pdf.y;
                            pdf.rect(75, pdf.y, 460, 1).stroke();
                            pdf.moveDown();
                            pdf.moveDown();
                            pdf.font('Times-Bold').text('PRIOR TO DOCS', 200, currentLevel + 5, { underline: true });
                            pdf.font('Times-Bold').text('Date Cleared', 400, currentLevel + 5);
                            pdf.font('Times-Bold').text('Cleared By', 475, currentLevel + 5);
                            pdf.moveDown();
                            for (x = 0; x < docsCond.length; x++) {
                                var loopLevel = pdf.y;
                                pdf.font('Times-Roman').text(docsCond[x].conditionNumber, 75, loopLevel, { width: 20 })
                                pdf.font('Times-Roman').text(docsCond[x].message, 100, loopLevel, { width: 300 })
                                pdf.rect(400, loopLevel + pdf.heightOfString(docsCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.rect(475, loopLevel + pdf.heightOfString(docsCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.moveDown();
                                if (pdf.y > 600) {
                                    pdf.addPage();
                                }
                            }
                        }

                        if (uwReviewCond.length > 0) {
                            var currentLevel = pdf.y;
                            pdf.rect(75, pdf.y, 460, 1).stroke();
                            pdf.moveDown();
                            pdf.moveDown();
                            pdf.font('Times-Bold').text('UW TO REVIEW', 200, currentLevel + 5, { underline: true });
                            pdf.font('Times-Bold').text('Date Cleared', 400, currentLevel + 5);
                            pdf.font('Times-Bold').text('Cleared By', 475, currentLevel + 5);
                            pdf.moveDown();
                            for (x = 0; x < uwReviewCond.length; x++) {
                                var loopLevel = pdf.y;
                                pdf.font('Times-Roman').text(uwReviewCond[x].conditionNumber, 75, loopLevel, { width: 20 })
                                pdf.font('Times-Roman').text(uwReviewCond[x].message, 100, loopLevel, { width: 300 })
                                pdf.rect(400, loopLevel + pdf.heightOfString(uwReviewCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.rect(475, loopLevel + pdf.heightOfString(uwReviewCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.moveDown();
                                if (pdf.y > 600) {
                                    pdf.addPage();
                                }
                            }
                        }

                        if (fundCond.length > 0) {
                            var currentLevel = pdf.y;
                            pdf.rect(75, pdf.y, 460, 1).stroke();
                            pdf.moveDown();
                            pdf.moveDown();
                            pdf.font('Times-Bold').text('Fund', 200, currentLevel + 5, { underline: true });
                            pdf.font('Times-Bold').text('Date Cleared', 400, currentLevel + 5);
                            pdf.font('Times-Bold').text('Cleared By', 475, currentLevel + 5);
                            pdf.moveDown();
                            for (x = 0; x < fundCond.length; x++) {
                                var loopLevel = pdf.y;
                                pdf.font('Times-Roman').text(fundCond[x].conditionNumber, 75, loopLevel, { width: 20 })
                                pdf.font('Times-Roman').text(fundCond[x].message, 100, loopLevel, { width: 300 })
                                pdf.rect(400, loopLevel + pdf.heightOfString(fundCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.rect(475, loopLevel + pdf.heightOfString(fundCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.moveDown();
                                if (pdf.y > 600) {
                                    pdf.addPage();
                                }
                            }
                        }

                        if (discCond.length > 0) {
                            var currentLevel = pdf.y;
                            pdf.rect(75, pdf.y, 460, 1).stroke();
                            pdf.moveDown();
                            pdf.moveDown();
                            pdf.font('Times-Bold').text('DISCLOSURES', 200, currentLevel + 5, { underline: true });
                            pdf.font('Times-Bold').text('Date Cleared', 400, currentLevel + 5);
                            pdf.font('Times-Bold').text('Cleared By', 475, currentLevel + 5);
                            pdf.moveDown();
                            for (x = 0; x < discCond.length; x++) {
                                var loopLevel = pdf.y;
                                pdf.font('Times-Roman').text(discCond[x].conditionNumber, 75, loopLevel, { width: 20 })
                                pdf.font('Times-Roman').text(discCond[x].message, 100, loopLevel, { width: 300 })
                                pdf.rect(400, loopLevel + pdf.heightOfString(discCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.rect(475, loopLevel + pdf.heightOfString(discCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.moveDown();
                                if (pdf.y > 600) {
                                    pdf.addPage();
                                }
                            }
                        }

                        if (miscCond.length > 0) {
                            var currentLevel = pdf.y;
                            pdf.rect(75, pdf.y, 460, 1).stroke();
                            pdf.moveDown();
                            pdf.moveDown();
                            pdf.font('Times-Bold').text('MISC', 200, currentLevel + 5, { underline: true });
                            pdf.font('Times-Bold').text('Date Cleared', 400, currentLevel + 5);
                            pdf.font('Times-Bold').text('Cleared By', 475, currentLevel + 5);
                            pdf.moveDown();
                            for (x = 0; x < miscCond.length; x++) {
                                var loopLevel = pdf.y;
                                pdf.font('Times-Roman').text(miscCond[x].conditionNumber, 75, loopLevel, { width: 20 })
                                pdf.font('Times-Roman').text(miscCond[x].message, 100, loopLevel, { width: 300 })
                                pdf.rect(400, loopLevel + pdf.heightOfString(miscCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.rect(475, loopLevel + pdf.heightOfString(miscCond[x].message, { width: 300 }), 50, 1).stroke();
                                pdf.moveDown();
                                if (pdf.y > 600) {
                                    pdf.addPage();
                                }
                            }
                        }

                        pdf.end();

                        //console.log(result.LOXmlFormat.applicant);
                        //console.log(informationHolder);
                    })
                }
            })
        })
    })
})