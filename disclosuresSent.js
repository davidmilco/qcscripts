//Dependencies needed contained in node_modules
var http = require('http');
var AWS = require('aws-sdk');
AWS.config.update({ region: 'us-west-2' });
AWS.config.accessKeyId = 'AKIAJRKU33GTOBCDLIZQ';
AWS.config.secretAccessKey = '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';
var nodemailer = require('nodemailer');

//Constant Variables provided by LQB
var reportUrl = 'http://api.openmtg.com/customReport/Disclosure%20Email%20Notice';
var requestDataString ='';



console.log('disclosuresSnet Automation Function Run');

let transporter = nodemailer.createTransport({
    SES: new AWS.SES({
        apiVersion: '2010-12-01'
    })
});

http.get(reportUrl, function (res) {
    res.on('data', function (chunkBuffer) {
        requestDataString += chunkBuffer.toString();
    });
    res.on('end', function () {
        console.log(requestDataString);
        var splitLinesArray = requestDataString.split('\n');
        var removedTopLines = splitLinesArray.splice(4, splitLinesArray.length);
        var tempHolder = [];
        //For each line split it into an array with each value being the column data.  Grab the loan number and put into a tempHolder array.
        for (var y = 0; y < removedTopLines.length; y++) {
            var singleLine = removedTopLines[y].split(',');
            tempHolder.push({
                loanNumber: singleLine[0].replace(/['"\r]+/g, ''),
                date: singleLine[1].replace(/['"\r]+/g, ''),
                lo:singleLine[2].replace(/['"\r]+/g, ''),
                loPhone:singleLine[3].replace(/['"\r]+/g, ''),
                loa:singleLine[4].replace(/['"\r]+/g, ''),
                loaPhone:singleLine[5].replace(/['"\r]+/g, ''),
                processor:singleLine[6].replace(/['"\r]+/g, ''),
                processorPhone:singleLine[7].replace(/['"\r]+/g, ''),
                borrEmail:singleLine[8].replace(/['"\r]+/g, ''),
                coboEmail:singleLine[9].replace(/['"\r]+/g, '')
            })
        }
        console.log(tempHolder);
        //Case where no loans are found from the report.
        if (tempHolder.length === 0) {
            console.log('No data found to work with');
            return
        } else{
            //generateDocAndEmail(tempHolder[0]);
            tempHolder.forEach(element => {
                generateDocAndEmail(element);
            });
        }

    })
})

var generateDocAndEmail = function(loanInfo){
    console.log('Generating Doc and Email Function Exectued with:');
    console.log(loanInfo);
    //var mailList = 'StacyB@corp.openmtg.com';
    //var mailList = 'davidmilco@corp.openmtg.com';
    var mailList = loanInfo.borrEmail;
    var contactString = '';
    if(loanInfo.lo){
        contactString += 'Loan Officer: ' + loanInfo.lo + ' ' + loanInfo.loPhone +' <br>';
    }
    if(loanInfo.loa){
        contactString += 'Loan Officer Assistant: ' + loanInfo.loa + ' ' + loanInfo.loaPhone +' <br>';
    }
    if(loanInfo.processor){
        contactString += 'Processor: ' + loanInfo.processor + ' ' + loanInfo.processorPhone +' <br>';
    }
    transporter.sendMail({
        from: 'no-reply@openmortgage.com',
        to: mailList,
        //to: 'davidmilco@openmortgage.com',
        subject: 'Your Home Loan Disclosures',
        html: `<!DOCTYPE html><html>
        <body>
        <table style="width:100%">
        <tr style="font-size: 20px;">
            <th><u>Action Required</u></th>
        </tr>
        </table>
        <br>
        <p style="font-size: 18px;">
         Thank you for choosing Open Mortgage, LLC.  On `+ loanInfo.date +` a set of documents was sent to you that require action on your part in order for us to proceed with your loan application.
         <br><br><br>
         Please follow the instructions in the document package to e-sign the forms.  If you have already completed the document package, please disregard this message.  
         <br><br><br>
         If for any reason you are unable to execute the documents and would like to expedite your application please contact your Loan Officer, their Assistant, if applicable, or your Loan Processor to indicate verbally that you wish to proceed with your mortgage loan application and will accept responsibility for costs incurred for documents required for loan approval.
         <br><br><br>
        `+ contactString +`
        </p>
        </body></html>
        `,
        ses: { // optional extra arguments for SendRawEmail
            Tags: [{
                Name: 'tagName',
                Value: 'tagValue'
            }]
        }
    }, function(err, info) {
        if(err){
            console.log('Error sending Email:');
            console.log(err);
        } else{
            console.log('Sent Email successfully');
            console.log(info);
        }
    }
    )
}